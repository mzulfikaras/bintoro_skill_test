<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Models\User;

class UserController extends Controller
{
    public function show()
    {
        try{
            $res = UserService::showuser();

            return response()->json([
                'success' => true,
                'message' => 'success show data!',
                'data' => $res
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'data' => [],
                'error_msg' => $e->getMessage()
            ], 500);
        }

    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required|max:50',
            'birth_date' => 'required',
            'birth_place' => 'required' ,
            'gender' => 'required'
        ]);

        try {

            $res = UserService::createuser($validate);

            return response()->json([
                'success' => true,
                'message' => 'success create data!',
                'data' => $res
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'data' => [],
                'error_msg' => $e->getMessage()
            ], 500);
        }
    }

    public function edit(Request $request,$id){
        try {

            $res = UserService::edituser($id, $request->all());

            return response()->json([
                'success' => $res,
                'message' => 'success edit data!'
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'error_msg' => $e->getMessage()
            ], 500);
        }
    }

    public function delete($id){
        try {

            $res = UserService::deleteuser($id);

            return response()->json([
                'success' => $res,
                'message' => 'success delete data!',
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'error_msg' => $e->getMessage()
            ], 500);
        }
    }

    public function sort(){
        try {

            $res = UserService::sortuser();

            return response()->json([
                'success' => true,
                'message' => 'success get data!',
                'data' => $res            
            ], 200);

        } catch (\Throwable $e) {
            return response()->json([
                'success' => false,
                'data' => [],
                'error_msg' => $e->getMessage()
            ], 500);
        }
    }
}
