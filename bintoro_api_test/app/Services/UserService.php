<?php

namespace App\Services;
use App\Models\User;

class UserService {

	public static function showuser(){
		$data = User::all();

		return $data;
	}

	public static function createuser($requestall)
	{
		$data = User::create($requestall);

		return $data;
	}

	public static function edituser($id, $requestall)
	{	
		$findUser = User::find($id);

		$data = $findUser->update($requestall);

		return $data;
	}

	public static function deleteuser($id){
		$findUser = User::find($id);

		$data = $findUser->delete();

		return $data;
	}

	public static function sortuser(){
		$collection = User::all();
		$sortDate = $collection->sortBy('birth_date');
		$data = $sortDate
				->groupBy('birth_place')
				->map(function($users, $birth_place){
					$result = [];
					foreach($users as $key => $u){
						$result[] = [
							'id' => $u->id,
							'name' => $u->name,
							'birth_date' => $u->birth_date,
							'gender' => $u->gender
						] ;
					}
					return [
						'birth_place' => $birth_place,
						'jumlah_data_user' => $users->count(),
						'users' => $result
					];
				})
				->values()
				->all();

		return $data;
	}
}	