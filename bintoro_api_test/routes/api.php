<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('user')->group(function (){
    Route::get('/', [UserController::class, 'show']);
    Route::post('/create', [UserController::class, 'store']);
    Route::put('/edit/{id}', [UserController::class, 'edit']);
    Route::delete('/delete/{id}', [UserController::class, 'delete']);
    Route::get('/sort', [UserController::class, 'sort']);
});